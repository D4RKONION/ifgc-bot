const { SlashCommandBuilder } = require('discord.js');
const fs = require('fs');
const https = require('https');
const { resize } = require('imagemagick');
console.log(resize)

module.exports = {
  data: new SlashCommandBuilder()
    .setName('events')
    .setDescription('Parses #events-and-news and refreshes fgcireland.com'),
  async execute(interaction) {
    const allowedRoles = ["Event Setter", "Community Mod"];
    const hasPermission = interaction.member.roles.cache.some(r => 
      allowedRoles.some(role => r.name.includes(role))
    );
    
    if (!hasPermission) {
      return interaction.reply("Sorry, you don't have permission to use that command :(");
    }


    console.log("I'm gonna collect the events");
    const eventChannel = interaction.client.channels.cache.get("778019953160224768");
    const outputFilename = './events.json';
    const eventsData = [];
    const imagesPath = "./images";
    const imageList = [];
    
    eventChannel.messages.fetch({ limit: 100 }).then(messages => {
      console.log(`Received ${messages.size} messages`);
      messages.forEach(async (message) => {
        if (!isValidEventMessage(message.content)) {
          console.log(`Something went wrong with ${message.content.substring(message.content.indexOf("Name: ") + 6, message.content.indexOf("Game: ") -1)}`)
          return;
        }
        
        const eventDetails = extractEventDetails(message.content);
        const attachment = [...message.attachments.values()][0];
        
        if (attachment) {
          eventDetails.imgUrl = generateFileName(eventDetails.name, eventDetails.date);
          imageList.push(eventDetails.imgUrl)
          if (!fs.existsSync(`${imagesPath}/${eventDetails.imgUrl}`)) {
            downloadImage(attachment.proxyURL, `${imagesPath}/${eventDetails.imgUrl}`);
          }
        }
        
        eventsData.push(eventDetails);
      });

      fs.writeFile(outputFilename, JSON.stringify(eventsData, null, 2), err => {
        if (err) {
          console.error("Error saving JSON:", err);
        } else {
          console.log(`JSON saved to ${outputFilename}`);
          interaction.reply("I've saved the new events JSON");
        }
      });

      const allImageFiles = fs.readdirSync(imagesPath);

      allImageFiles.forEach(fileName => {
        console.log(fileName)
        if (!imageList.includes(fileName)) {

        console.log("attempting delete")
          try {
            fs.unlinkSync(`${imagesPath}/${fileName}`); // Delete the file
          } catch (err) {
            console.error(`Error deleting file: ${err}`)
          }
        }
      })
      

    });
  }
};

// Helper functions
function isValidEventMessage(content) {
  return ["Name:", "Date:", "Time:", "Game:", "Where:", "Signup:", "Stream:", "Details:"].every(field => content.includes(field));
}

function extractEventDetails(content) {
  return {
    name: extractField(content, "Name:", "Game:"),
    game: extractField(content, "Game:", "Date:"),
    date: extractField(content, "Date:", "Time:"),
    time: extractField(content, "Time:", "Where:"),
    where: extractField(content, "Where:", "Signup:"),
    signup: extractField(content, "Signup:", "Stream:"),
    stream: extractField(content, "Stream:", "Details:"),
    details: content.substring(content.indexOf("Details:") + 9).trim()
  };
}

function extractField(content, start, end) {
  return content.substring(content.indexOf(start) + start.length, content.indexOf(end)).trim();
}

function generateFileName(name, date) {
  return `${sanitizeString(name)}-${sanitizeString(date)}.png`;
}

function sanitizeString(str) {
  return str.toLowerCase().replace(/[<>:"/\\|?*&#]/g, "").replaceAll(" ", "-");
}

function downloadImage(url, path) {
  https.get(url, res => {
    const fileStream = fs.createWriteStream(path);
    res.pipe(fileStream);
    fileStream.on("finish", () => {
      fileStream.close(() => resize({
        srcPath: path,
        dstPath: path,
        width: 400
      }));
      console.log("Downloaded " + path);
      
    });
    fileStream.on("error", err => {
      console.error("Error downloading file:", err);
    });
    return { success: true };
  });
}
